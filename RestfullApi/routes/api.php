<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * buyer
 */
Route::resource('Buyers','Buyer\BuyerController',['only'=>['index','show']]);

/*
 * Category
 */
Route::resource('Categories','Category\CategoryController',['except'=>['create','edit']]);

/*
 * Product
 */
Route::resource('Products','Product\ProductController',['only'=>['index','show']]);

/*
 * Seller
 */
Route::resource('Sellers','Seller\SellerController',['only'=>['index','show']]);

/*
 * Transaction
 */
Route::resource('Transactions','Transaction\TransactionController',['only'=>['index','show']]);

/*
 * User
 */
Route::resource('Users','User\UserController',['except'=>['create','update']]);
