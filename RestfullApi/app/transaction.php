<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaction extends Model
{
    //
    protected $fillable = [
        'quantity',
        'buyer_id',
        'product_id'
    ];

    public function product(){
        return $this->belongsTo(product::class);
    }

    public function buyer(){
        return $this->belongsTo(buyer::class);
    }
}
