<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    public function categories(){
        return $this->belongsToMany(category::class);
    }

    public function seller(){
        return $this->belongsTo(seller::class);
    }

    public function transactions(){
        return $this->hasMany(transaction::class);
    }

    //
    const AVAILABLE_PRODUCT = "available";
    const UNAVAILABLE_PRODUCT = "unavailable";
    protected $fillable =[
        'name',
        'description',
        'quantity',
        'status',
        'image',
        'seller_id',
    ];

    public function isAvailable(){
        return $this->status =product::AVAILABLE_PRODUCT;
    }
}
